<?php
 include 'head.html';
  ini_set('default_charset', 'UTF-8');
  error_reporting(E_ERROR | E_PARSE);
?>

<body>

<?php 
  include 'header.php';
  //include 'login/verifySessionStarted.php';
?>

<main id="main">
  <section class="breadcrumbs">
    <div class="container">
      <div class="d-flex justify-content-between align-items-center">
        <ol>
          <li><a href="index.php">Início</a></li>
          <li><a href="formFaq.php">Registrar Assinatura</a></li>
        </ol>
      </div>
    </div>
  </section>

  <section>
  <div class="section-title aos-init aos-animate" data-aos="fade-up">
    <h2>Cadastrar nova Assinatura</h2>
  </div>

  <div class="div-space">
    <form class="container" name="registar" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
      <label>Nome: </label>
      <div class="input"><input class="form-control" type="text" name="nome" maxlength="32" value=""></div><br>
      <label>Cargo: </label>
      <div class="input"><input class="form-control" type="text" name="cargo" maxlength="32" value=""></div><br>
      <label>Ramal: </label>
      <div class="input"><input class="form-control" type="text" name="fone" maxlength="5" value=""></div><br>
      <label>Telefone: </label>
      <div class="input"><input class="form-control" type="text" name="cel" maxlength="15" value=""></div><br>
      <input class="button" name="submit" type="submit" value="Criar" /><br>
    </form>
  </div>

</section>
  <div class="div-space">
    <div class="container">
      <?php include 'exibe_assinatura.php'?>
    </div>
  </div>
</main>
</div>

<?php 
  include 'footer.php';
?>
</body>
</html>