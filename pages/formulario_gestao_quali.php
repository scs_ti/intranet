<?php include 'head.html'?>
<body>
  <?php include 'header.php';
  include 'login/verifySessionStarted.php';
  // include 'login/lvl_access.php';
  date_default_timezone_set('UTC');
  ?>
  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="formComunicados.php">Arquivos Gestão da Qualidade</a></li>
          </ol>
        </div>
      </div>
    </section>
    <section id="faq" class="faq">
      <div class="section-title aos-init aos-animate" data-aos="fade-up">
        <h2>Arquivos Gestão da Qualidade</h2>
      </div>
      <div class="container">
        <div class="section-title" data-aos="fade-up"></div>          
        <form action="contact/contact_gestao_quali.php" method="POST" enctype="multipart/form-data">
          <div class="form-group">
            <label>Titulo:</label>
            <input type="text" name="titulo" class="form-control" required="required" id="titulo" value=""><br>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">Categoria</label>
            <select name="categoria" class="form-control" id="exampleFormControlSelect1">
              <option>Selecione</option>
              <option>Politicas Institucionais</option>
              <option>POP</option>
              <option>Manual</option>
              <option>SIPOC</option>
              <option>Fluxogramas</option>
              <option>Formularios para Download</option>
              <option>Treinamento Qualidade</option>
              <option>Formulários</option>
            </select>
          </div><br>
          <div class="form-group">
            <label for="exampleFormControlSelect1">Setor</label>
            <select name="setor" class="form-control" id="exampleFormControlSelect1">
              <option>Selecione</option>
              <option>RH</option>
              <option>Financeiro</option>
              <option>Qualidade</option>
              <option>Ouvidoria</option>
              <option>Atendimento</option>
              <option>Relacionamento Empresarial</option>
              <option>Auditoria</option>
            </select>
          </div><br>
          <div class="form-group">
            <label for="conteudo">Enviar Arquivo</label>
            <input class="form-control" type="file" name="fileToUpload" id="fileToUpload">
          </div><br>
          <div class="form-group">
            <input type="text" name="autor" hidden class="form-control" readonly="readonly" id="autor" value="<?php echo($_SESSION['usuario']);?>"><br>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Salvar</button>
            <button type="button"  class="btn btn-danger"><a href="index.php" style="color: #fff">Cancelar</a></button>
          </div>
        </form>
      </div>
    </section>
  </section>
</main>
<div style="padding-top: 6%">
  <?php include 'footer.php' ?>
</div>
</body>
</html>

