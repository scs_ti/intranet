<?php include 'head.html';
ini_set('default_charset', 'UTF-8');
?>

<body>

  <?php include 'header.php';
  include 'login/verifySessionStarted.php';
  //include 'login/lvl_access_quali.php';
  ?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="formFaq.php">Registrar Elogios</a></li>
          </ol>
        </div>
      </div>
    </section>
    <section id="faq" class="faq">
      <div class="section-title aos-init aos-animate" data-aos="fade-up">
        <h2>Cadastro de Elogios</h2>
      </div>
      <div class="container">
        <div class="section-title" data-aos="fade-up"></div>
        <form action="contact/contact_elogio.php" method="POST" enctype="multipart/form-data">
          <div class="form-group">
            <label>Nome do beneficiário:</label>
            <input type="text" name="autor" class="form-control" required="required" id="autor" value=""><br>
          </div>
          <div class="form-group">
            <label>Data do Elogio:</label>
            <input type="date" name="data_elogio" class="form-control" required="required" id="data" value=""><br>
          </div>
          <div class="form-group">
            <label>Canal de Entrada:</label>
            <input type="text" name="entrada" class="form-control" required="required" id="entrada" value=""><br>
          </div>
          <div class="form-group">
            <label>Setor Elogiado:</label>
            <input type="text" name="setor" class="form-control" required="required" id="setor" value=""><br>
          </div>
          <div class="form-group">
            <label>Elogio Direcionado:</label>
            <input type="text" name="destino" class="form-control" required="required" id="destino" value=""><br>
          </div>
          <div class="form-group">
            <label for="conteudo">Conteúdo</label>
            <textarea class="form-control" name="conteudo" id="conteudo" rows="3" maxlength="3000" required="required"></textarea>
          </div><br>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Salvar</button>
            <button type="button"  class="btn btn-danger"><a href="index.php" style="color: #fff">Cancelar</a></button>
          </div>
        </form>
      </div>
    </section>
  </section>
</main>
</div>
<?php include 'footer.php' ?>
</body>
</html>