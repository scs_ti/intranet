<?php include 'head.html'?>

<body>
  <?php include 'header.php';
  //include 'login/verifySessionStarted.php';
  ?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="aniversariantes.php">Aniversariantes por tempo de Empresa</a></li>
          </ol>
        </div>
      </div>
    </section>
    <section id="team" class="team section-bg">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Aniversariantes por tempo de Empresa do Dia</h2>
        </div>
        <div class="row">
          <?php 

          $hoje = date('d/m');
          $ano = date('y');
          if (!isset($_POST['submit']) or strlen($_POST['oculto']) == 0) {
            $sql = "SELECT extract(month from DT_NASCIMENTO) AS mes, extract(day from DT_NASCIMENTO) AS DAY, extract(year from DT_NASCIMENTO) AS ano, COLABORADOR, LOTACAO, RAMAL, CD_USUARIO, DS_EMAIL, ADMISSAO FROM info_users
            WHERE SUBSTR(ADMISSAO , 1,5) = '".$hoje."' AND SUBSTR(ADMISSAO , 7) != '20".$ano."' ";

            $stid = oci_parse($ora_conexao, $sql) or die ("erro");
            oci_execute($stid); 

            while (oci_fetch($stid)) {

              if(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") < 9){
                $data = oci_result($stid, "ANO")."-0".oci_result($stid, "MES")."-0".oci_result($stid, "DAY");
              }elseif(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") > 9){
                $data = oci_result($stid, "ANO")."-0".oci_result($stid, "MES")."-".oci_result($stid, "DAY");
              }elseif(oci_result($stid, "MES") > 9 and oci_result($stid, "DAY") < 9){
                $data = oci_result($stid, "ANO")."-0".oci_result($stid, "MES")."-0".oci_result($stid, "DAY");
              }else{
                $data = oci_result($stid, "ANO")."-".oci_result($stid, "MES")."-".oci_result($stid, "DAY");
              }

              $date = explode('-', $data);

              $locacao = explode(' ', oci_result($stid, "LOTACAO"));
              $foto = "../files/usuarios/".oci_result($stid, 'CD_USUARIO').".jpg";
              echo "
              <div class='col-lg-4 col-md-6 d-flex align-items-stretch'>
              <div class='member' data-aos='fade-up' data-aos-delay='100'>
              <div class='member-img'>"?>
              <?php 
              if(file_exists($foto)){
                echo "<img src='".$foto."' class='img-fluid img-fluid-2' style='max-width: 50%; padding-top: 15px; border-radius: 50%;' alt=''>";
              }else{
                echo "<img src='https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png' class='img-fluid-2' style='max-width: 50%;' alt=''>";
              }
              ?>
              <?php echo "</div>
              <div class='member-info'>
              <h4>".oci_result($stid, "COLABORADOR")."</h4>
              <span>Email: ".strtolower(oci_result($stid, "DS_EMAIL"))."</span>
              <span>Data de admissão: ".oci_result($stid, "ADMISSAO")."</span>
              <span>Data de Nascimento: ".$date[2]."/".$date[1]."/".$date[0]."</span>
              <span>Ramal: ".oci_result($stid, "RAMAL")."</span>
              <span>Setor: ".strtolower($locacao[1])."</span>
              </div>
              </div>
              </div>";
            }
          }
          ?>
        </div>
      </section>
      <section id="team" class="team section-bg">
        <div class="container">
          <div class="section-title" data-aos="fade-up">
            <h2>Aniversariantes por tempo de Empresa do Mês</h2>
          </div>
          <div class="row">
            <?php 
            $mes = date('m');
            if (!isset($_POST['submit']) or strlen($_POST['oculto']) == 0) {
              $sql = "SELECT extract(month from DT_NASCIMENTO) AS mes, extract(day from DT_NASCIMENTO) AS DAY, extract(year from DT_NASCIMENTO) AS ano, COLABORADOR, LOTACAO, RAMAL, CD_USUARIO, DS_EMAIL, ADMISSAO FROM info_users
              WHERE SUBSTR(ADMISSAO , 3,4) = '/".$mes."/' AND SUBSTR(ADMISSAO , 7) != '20".$ano."' ";
              $stid = oci_parse($ora_conexao, $sql) or die ("erro");
              oci_execute($stid); 

              while (oci_fetch($stid)) {


                if(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") < 9){
                  $data = oci_result($stid, "ANO")."-0".oci_result($stid, "MES")."-0".oci_result($stid, "DAY");
                }elseif(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") > 9){
                  $data = oci_result($stid, "ANO")."-0".oci_result($stid, "MES")."-".oci_result($stid, "DAY");
                }elseif(oci_result($stid, "MES") > 9 and oci_result($stid, "DAY") < 9){
                  $data = oci_result($stid, "ANO")."-0".oci_result($stid, "MES")."-0".oci_result($stid, "DAY");
                }else{
                  $data = oci_result($stid, "ANO")."-".oci_result($stid, "MES")."-".oci_result($stid, "DAY");
                }

                $date = explode('-', $data);


                $locacao = explode(' ', oci_result($stid, "LOTACAO"));
                $foto = "../files/usuarios/".oci_result($stid, 'CD_USUARIO').".jpg";
                echo "
                <div class='col-lg-4 col-md-6 d-flex align-items-stretch'>
                <div class='member' data-aos='fade-up' data-aos-delay='100'>
                <div class='member-img'>"?>
                <?php 
                if(file_exists($foto)){
                  echo "<img src='".$foto."' class='img-fluid img-fluid-2' style='max-width: 50%; padding-top: 15px; border-radius: 50%;' alt=''>";
                }else{
                  echo "<img src='https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png' class='img-fluid-2' style='max-width: 50%;' alt=''>";
                }
                ?>
                <?php 

                echo "</div>
                <div class='member-info'>
                <h4>".oci_result($stid, "COLABORADOR")."</h4>
                <span>Email: ".strtolower(oci_result($stid, "DS_EMAIL"))."</span>
                <span>Data de admissão: ".oci_result($stid, "ADMISSAO")."</span>
                <span>Data de Nascimento: ".$date[2]."/".$date[1]."/".$date[0]."</span>
                <span>Ramal: ".oci_result($stid, "RAMAL")."</span>
                <span>Setor: ".strtolower($locacao[1])."</span>
                </div>
                </div>
                </div>";
              }
            }
            ?>
          </div>
          <div class="col-6 col-sm-4">
            <button type="button"  class="btn btn-primary">
              <a href="index.php" style="color: #fff">Voltar</a>
            </button>
          </div>
        </section>
      </main>
    </div>
    <?php include 'footer.php' ?>
  </body>
  </html>