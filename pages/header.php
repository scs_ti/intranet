<?php 
include '../actions/conecta.php';
session_start();
?>

<header id="header" class="fixed-top d-flex align-items-center" style="box-shadow: 0px 2px 15px rgb(0 0 0 / 10%); padding: 10px 0;">
  <div class="container d-flex align-items-center justify-content-between">
    <div class="logo">
      <a href="index.php"><img src="https://www.santacasasaude-planos.com/img/santa-casa-saude-logo.png" alt="" 
       class="img-fluid"></a>
     </div>

     <nav id="navbar" class="navbar">
      <ul>
        <li><a class="nav-link scrollto active" href="index.php">Inicio</a></li>
        <li><a class="nav-link scrollto" href="faq.php">FAQ</a></li>
        <li><a class="nav-link scrollto" href="index.php#contact">Contato</a></li>
        <?php 
        if(isset($_SESSION['usuario'])) {
          $stid = oci_parse($ora_conexao, "SELECT * FROM info_users where CD_USUARIO like UPPER('".$_SESSION['usuario']."')");
          oci_execute($stid); 
          while (oci_fetch($stid)) {
            $name = ucwords(strtolower(oci_result($stid, "COLABORADOR")));
            echo("<li><a href='perfil.php'>Bem-vindo(a) ".$name."</a></li>
              <li><a class='getstarted scrollto' href='index.php'>Intranet</a></li>
              <li><a class='getstarted scrollto' href='login/logout.php'>Sair</a></li>");
          }
        }
        else {
          echo "<li><a class='getstarted scrollto' href='login/login_automatico.php'>Entrar</a></li>";
        }
        ?>
      </ul>
      <i class="bi bi-list mobile-nav-toggle"></i>
    </nav>
  </div>
</header>