<!DOCTYPE html>
<html lang="en">
<?php include 'head.html'?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css" rel="stylesheet">
<script src="../assets/js/modal.js"></script>
<body>

  <?php 
  	include 'header.php';
  	include 'login/verifySessionStarted.php';

  	$consulta_elogio = "SELECT * FROM DBAPS.elogio";
	$stid_elogio = oci_parse($ora_conexao, $consulta_elogio) or die ("erro");
	oci_execute($stid_elogio);
  ?>

  <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="faq.php">Elogios</a></li>
          </ol>
        </div>
      </div>
  </section>
  
  <br>
  <div class="section-title aos-init aos-animate" data-aos="fade-up">
      <h2>Elogios</h2>
  </div>

  <div class="container">
  <div class="card p-3">
    <table class="table table-striped" id="minhaTabela">
      <thead>
        <tr>
          <th>Autor</th>
          <th>Data do Elogio</th>
          <th>Canal de Entrada</th>
          <th>Setor Elogiado</th>
          <th>Elogio Direcionado</th>
          <th>Visualizar</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          while (($row = oci_fetch_array($stid_elogio, OCI_BOTH)) != false) {
            echo "<tr>
                    <td>".
                      $row['AUTOR'].
                    "</td>
                    <td>".
                      date('d/m/Y', strtotime($row['DATA_ELOGIO'])).
                    "</td>
                    <td>".
                      $row['CANAL_ENTRADA'].
                    "</td>
                    <td>".
                      $row['SETOR_ELOGIO'].
                    "</td>
                    <td>".
                      $row['ELOGIO_DIRECIONADO'].
                    "</td>
                    <td>
                      <a href='elogio_completo.php?id=".$row['ID_ELOGIO']."' class='btn btn-primary'>Visualizar</a>
                    </td>
                  </tr>"
                ;
          }
        ?>
      </tbody>
    </table>
  </div>
  </div>
  
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>

  <script>
  $(document).ready(function(){
      $('#minhaTabela').DataTable({
        	"language": {
                "lengthMenu": "Mostrando _MENU_ registros por página",
                "zeroRecords": "Nada encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhum registro disponível",
                "infoFiltered": "(filtrado de _MAX_ registros no total)"
            }
        });
  });
  </script>
  <div style="padding-top: 5%">
  	<?php include 'footer.php' ?>
  </div>
</body>
</html>