    <section id="contact" class="contact">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Contato</h2>
        </div>
        <div class="row">
          <div class="col-md-9 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="contact-about">
              <h3>Plano Santa Casa Saúde</h3>
              <div class="social-links">
                <a href="https://www.instagram.com/plano_santacasasaude/" class="instagram"><i class="bi bi-instagram"></i></a>
                <a href="https://www.linkedin.com/company/plano-santa-casa-sa%C3%BAde/mycompany/" class="linkedin"><i class="bi bi-linkedin"></i></a>
              </div>
            </div>
          </div>
          <div class="col-md-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="info">
              <div>
                <i class="ri-map-pin-line"></i>
                <p>Rua Eugênio Bonadio, 63<br>Centro, São José dos Campos</p>
              </div>
              <div>
                <i class="ri-mail-send-line"></i>
                <p>comunicacao@santacasasaudesjc.com.br</p>
              </div>
              <div>
                <i class="ri-phone-line"></i>
                <p>(12) 3876-9600</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
