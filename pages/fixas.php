<?php include 'head.html'?>

<body>
  <?php include '../actions/conecta.php'
  ?>

  <main id="main">
    <section id="team" class="team section-bg">
      <div class="container">
        <div class="row">
          <?php 
          if (!isset($_POST['submit']) or strlen($_POST['oculto']) == 0) {
            $sql = "SELECT * FROM V_VALORES_TABELA_SJC";
            $stid = oci_parse($ora_conexao, $sql) or die ("erro");
            oci_execute($stid); 
            while (oci_fetch($stid)) {
              echo"
              <div class='col-lg-4 col-md-5 d-flex align-items-stretch'>
              <div class='member' data-aos='fade-up' data-aos-delay='100'>
              <div class='member-info'>
              <h4>Registro ANS: ".ucwords(oci_result($stid, "REGISTRO ANS"))."</h4>
              <h6>Código: ".ucwords(oci_result($stid, "CODIGO TABELA"))."</h6>
              <h7>".ucwords(oci_result($stid, "NOME TABELA"))."</h7>
              <h6>Faixa: ".ucwords(oci_result($stid, "FAIXA"))."</h6 >
              <h6>Código Plano: ".ucwords(strtolower(oci_result($stid, "CODIGO PLANO")))."</h6>
              <h6>Variação: ".strtolower(oci_result($stid, "PORCENTAGEM"))."</h6>
              </div>
              </div>
              </div>";
            }
          }
        ?>
        </div>
      </section>
    </main>
  </div>
  <?php include 'footer.php' ?>
</body>
</html>