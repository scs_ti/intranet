<?php include 'head.html'?>

<body>

  <?php include 'header.php';
  include 'login/verifySessionStarted.php';
  // include 'login/lvl_access.php';
  ?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="registro_func.php">Registrar Funcionário</a></li>
          </ol>
        </div>
      </div>
    </section>

    <section id="team" class="team section-bg">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Registrar Funcionário</h2>
        </div>
        
        <div class="row">
          <form action="contact/register_user.php" method="POST">
            <?php 
            
            echo "<div class='form-group'>
            <label>Usuário de rede:</label>
            <input type='text' name='CD_USUARIO' class='form-control' id='CD_USUARIO' value=''><br>
            </div>
            <div class='form-group'>
            <label>Nome:</label>
            <input type='text' name='NM_USUARIO' class='form-control' id='NM_USUARIO' value=''><br>
            </div>
            <div class='form-group'>
            <label>Setor:</label>
            <select name='lotacao' class='form-control' id='DS_DEPTO_OPERADORA'>
            <option>000101 JURIDICO</option>
            <option>000103 TI</option>
            <option>000104 ATENDIMENTO/EMISSAO.GUIAS</option>
            <option>000106 TELEFONISTAS</option>
            <option>000108 CADASTRO</option>
            <option>000109 LIMPEZA</option>
            <option>000110 COMERCIAL</option>
            <option>000112 CONTABILIDADE</option>
            <option>000113 AUDITORIA</option>
            <option>000115 CONTAS MEDICAS</option>
            <option>000116 CREDENCIAMENTO</option>
            <option>000118 SAUDE PREVENTIVA</option>
            <option>000119 COBRANCA</option>
            <option>000121 FATURAMENTO</option>
            <option>000122 FINANCEIRO</option>
            <option>000124 OUVIDORIA</option>
            <option>000125 REGULACAO</option>
            <option>000127 ADM TAUBATE</option>
            <option>000128 CARAGUATATUBA</option>
            <option>000129 SAO SEBASTIAO</option>
            <option>000130 JACAREI</option>
            <option>000132 PINDAMONHANGABA</option>
            <option>000133 CRUZEIRO</option>
            <option>000134 RECURSOS HUMANOS</option>
            <option>000135 GUARATINGUETA</option>
            <option>00137 RECURSO DE GLOSA</option>
            <option>00139 CACHOEIRA PAULISTA</option>
            <option>00141 SECRETARIA</option>
            <option>00142 RELACIONAMENTO COM CLIENTE</option>
            <option>00143 SUPRIMENTOS</option>
            <option>00144 PARAMETRIZAÇÃO E ADEQUAÇÃO</option>
            </select>
            </div><br>";?>
            <div class='form-group'>
              <label>Ramal:</label>
              <input type='text' name='RAMAL' class='form-control' 
              oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1')" maxlength=4 id='RAMAL' value=''><br>
            </div>
            <?php echo "
            <div class='form-group'>
            <label>Data de Nascimento: </label>
            <input name='DT_NASCIMENTO' type='date' class='form-control' id='DT_NASCIMENTO' value=''><br>
            </div>
            <div class='form-group'>
            <label>Data de Admissão: </label>
            <input name='admissao' type='date' class='form-control' id='admissao' value=''><br>
            </div>
            <div class='form-group'>
            <label>Email:</label>
            <input type='text' name='DS_EMAIL' class='form-control' id='DS_EMAIL' value=''><br>
            </div>
            ";                
            ?>  
            <button type="submit" class="btn btn-success">Salvar</button>
            <button type="button"  class="btn btn-danger">
              <a href="index.php" style="color: #fff">Cancelar</a>
            </button>
          </form>
        </div>
      </section>
    </main>
  </div>

  <?php include 'footer.php' ?>

</body>
</html>