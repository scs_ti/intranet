﻿<?php include 'head.html'?>

<body>
  <?php include 'header.php';?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
  
  <main id="main" style="user-select: none;">
    <div class="d-flex align-items-center" style="padding: 0 0; padding-top: 80px;">
      <div class="container">
        <div class="p-4 p-md-5 mb-4 text-white rounded">
          <div class="col-md-6 px-0" data-aos="fade-up" >
            <img src="../assets/img/banner.PNG" style="width: 200%; position: absolute;">
          </div>
        </div>
      </div>
    </div>

    <div class="features" style="padding-top: 205px;">
      <div class="container">
        <div class="row" data-aos="fade-up" data-aos-delay="300">    
          <div class="col-lg-1 col-md-4">
            <!--<div class="icon-box" style="background: #0a7ac1; padding: 18px;">
              <h3>
                <a><strong style="color: white; font-size: 25px;">Parabéns para Quem?</strong></a>
              </h3>
            </div>-->
          </div> 
          <div class="col-lg-5 col-md-4">
            <div class="icon-box" style="background: #0a7ac1; padding: 18px;">
              <h3>
                <a><strong style="color: white; font-size: 25px;">Parabéns para Quem?</strong></a>
              </h3>
            </div>
            <div class="g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
              <div class="col p-4 d-flex flex-column position-static">
                <?php
                $sql = "SELECT extract(month from DT_NASCIMENTO) AS mes, extract(day from DT_NASCIMENTO) AS DAY, extract(year from DT_NASCIMENTO) AS ano, COLABORADOR, LOTACAO, RAMAL, CD_USUARIO, DS_EMAIL FROM info_users
                WHERE extract(month from DT_NASCIMENTO) = extract(month from sysdate)
                AND extract(day from DT_NASCIMENTO) = extract(day from sysdate)";
                $stid = oci_parse($ora_conexao, $sql) or die ("erro");
                oci_execute($stid); 

                while (oci_fetch($stid)) {
                  $locacao = explode(' ', oci_result($stid, "LOTACAO"));
                  echo '<h4 class="mb-0">'.ucfirst(ucwords(strtolower(oci_result($stid, "COLABORADOR")))).'</h4>';

                  if (oci_result($stid, "RAMAL") != 'NULL') {
                    echo '<p class="mb-1 text-muted">Ramal: '.oci_result($stid, "RAMAL").'</p>';
                  }
                  echo '<p class="mb-1 text-muted">Setor: '.ucfirst(strtolower($locacao[1])).'</p><br>';}?>
              </div>
              <img src="../assets/img/balao.png" style="width: 250px; float:right;">
            </div>
          </div>

          <div class="col-lg-5 col-md-4">
            <div class="icon-box" style="background: #41a4b5">
              <h3><a><strong style="color: white; font-size: 25px;">COMUNICADOS</strong></a></h3>
            </div>
            <div class="g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
              <div class="col p-4 d-flex flex-column position-static">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner" >        
                    <?php 
                      $sql1 = "SELECT * FROM comunicados
                      WHERE extract(month from data_criacao) = extract(month from sysdate)
                      AND extract(day from data_criacao) = extract(day from sysdate)";

                      $sql = " SELECT extract(month from DATA_CRIACAO) AS mes, 
                      extract(day from DATA_CRIACAO) AS DAY,
                      extract(year from DATA_CRIACAO) AS ano,
                      AUTHOR,
                      COD,
                      TITLE,
                      FILE_LOCAL,
                      CONTENT
                      FROM comunicados";

                      $stid = oci_parse($ora_conexao, $sql) or die ("erro");
                      oci_execute($stid); 

                      $x = 0;
                      while (oci_fetch($stid)){
                        
                        if ($x == 0) {
                          echo '<div class="item active">';
                        } else {
                          echo '<div class="item">';
                        }

                        if(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") < 9){
                          $data = "0".oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
                        }elseif(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") > 9){
                          $data = oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
                        }elseif(oci_result($stid, "MES") > 9 and oci_result($stid, "DAY") < 9){
                          $data = "0".oci_result($stid, "DAY")."/".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
                        }else{
                          $data = oci_result($stid, "DAY")."/".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
                        }
                        $file = explode('/', oci_result($stid, "FILE_LOCAL"));   
                        if (isset($file[7])){
                          echo "
                            <div class='col-lg-12'>
                              <h1 data-aos='fade-up'>".oci_result($stid, "TITLE")."</h1>
                              <h4 data-aos='fade-up' data-aos-delay='400'>".oci_result($stid, "CONTENT")."</h4>
                              <div data-aos='fade-up' data-aos-delay='800'>
                              <p class='mb-1 text-muted'>Data de Criação: ".$data."</p>
                              <a href='../".$file[5]."/".$file[6]."/".$file[7]."' target='_blank' class='btn-get-started scrollto'>Visualizar Anexo</a>
                            </div>
                          </div>
                        </div>";
                        }else{
                          echo "
                            <div class='col-lg-12'>
                              <h1 data-aos='fade-up'>".oci_result($stid, "TITLE")."</h1>
                              <h4 data-aos='fade-up' data-aos-delay='400'>".oci_result($stid, "CONTENT")."</h4>
                              <div data-aos='fade-up' data-aos-delay='800'>
                              <p class='mb-1 text-muted'>Data de Criação: ".$data."</p>
                              <a href='../files/".$file[5]."/".$file[6]."' target='_blank' class='btn-get-started scrollto'>Visualizar Anexo</a>
                            </div>
                          </div>
                        </div>";
                        }
                        
                        $x ++;
                      ;}?>
                      
                  </div>
                    <a class="carousel-control" href="#myCarousel" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div class="col-lg-1 col-md-4 mt-4 mt-md-0"></div>
      </div>
    </div>
      
    <center><section id="features" class="features" data-aos="fade-up">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
              <div class="icon-box" style="background: #41a4b5"><h3><a><strong style="color: white; font-size: 25px;">Missão</strong></a></h3></div><br>
                <p>Manter-se sustentável, dentro dos princípios éticos, gerando valor para a Rede de Saúde Santa Casa.</p>
              </div>
            </div><!-- /.col-lg-4 -->
            
            <div class="col-lg-4">
              <div class="g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                <div class="icon-box" style="background: #41a4b5"><h3><a><strong style="color: white; font-size: 25px;">Visão</strong></a></h3></div><br>
                  <table>
                    <tr>
                      <td align='justify'>Ser uma empresa de referência na área da Saúde,<br> com credibilidade e contínuo crescimento, <br>prezando pela excelência na prestação de serviçoes <br>e resultados.</td>
                    </tr>
                  </table>
                  <br>
                </div>
              </div><!-- /.col-lg-4 -->

              <div class="col-lg-4">
                <div class="g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                  <div class="icon-box" style="background: #41a4b5"><h3><a><strong style="color: white; font-size: 25px;">Valores</strong></a></h3></div><br>
                    <table>
                      <tr>
                        <td align='justify'><strong>Pontualidade</strong>: Cumprir os prazos estabelecidos.</td>
                      </tr>
                      <tr>
                        <td align='justify'><strong>Integridade</strong>: constância de propósitos e princípio.</td>
                      </tr>
                      <tr>
                        <td align='justify'><strong>Legalidade</strong>: constância de propósitos e princípio.</td>
                      </tr>
                      <tr>
                        <td align='justify'><strong>Humanização</strong>: constância de propósitos e princípio.</td>
                      </tr>
                      <tr>
                        <td align='justify'><strong>Credibilidade</strong>: Fazer com que nossos <br>clientes e prestadores confiem em nossos serviços.</td>
                      </tr>
                    </table>
                    <br>
                  </div>
                </div><!-- /.col-lg-4 -->
              </div><!-- /.row -->
          </div>
        </div>
      </div></center>
    </section>

    <?php

    if( !isset ($_SESSION['usuario']) ){
      $setor = "";
    }; 

    if( isset ($_SESSION['usuario']) ){
      $sql = "SELECT * FROM info_users WHERE CD_USUARIO LIKE UPPER('".$_SESSION['usuario']."')";
      $stid = oci_parse($ora_conexao, $sql) or die ("erro");
      oci_execute($stid);
      while (oci_fetch($stid)) {
        $setor = explode(" ", oci_result($stid, "LOTACAO"))[1];
      }
    }
  ?>
  
    <section id="features" class="features">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Recursos</h2>
        </div>
        <div class="row" data-aos="fade-up" data-aos-delay="300">
          
          <div class="col-lg-3 col-md-4">
            <div class="icon-box">
            <a href="soulMv.php"><img style="width: 25%" src="https://img.swapcard.com/?u=https%3A%2F%2Fcdn-api.swapcard.com%2Fpublic%2Fimages%2Fd791dcedc51a457eba46f38d4de301bd.png&q=0.8&m=fit&w=400&h=200"></img></a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="icon-box">
              <i class="ri-fingerprint-line" style="color: #2d4f6e;"></i>
              <h3><a href="perfil.php">Meu Perfil</a></h3>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <i class="fas fa-birthday-cake" style="color: #2d4f6e;"></i>
              <h3><a href="aniversariantes.php">Aniversariantes</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <i class="fas fa-birthday-cake" style="color: #2d4f6e;"></i>
              <h3><a href="aniversariantes_de_casa.php">Aniversariantes por tempo de Empresa</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-users" style="color: #2d4f6e;"></i>
              <h3><a href="usuarios.php">Lista de Colaboradores</a></h3>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="ri-file-list-3-line" style="color: #2d4f6e;"></i>
              <h3><a href="faq.php">FAQ</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-exclamation-diamond" style="color: #2d4f6e;"></i>
              <h3><a href="comunicados.php">Comunicados Internos</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-vector-pen" style="color: #2d4f6e;"></i>
              <h3><a href="https://rhmaster-prime.com.br/rhmaster.prime-2.0/home.jsf" target="_blank">RH Master</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-newspaper" style="color: #2d4f6e;"></i>
              <h3><a href="http://www.ans.gov.br/aans/noticias-ans" target="_blank">Notícias ANS</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-diagram-3" style="color: #2d4f6e;"></i>
              <h3><a href="">Organograma da Empresa</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-file-richtext" style="color: #2d4f6e;"></i>
              <h3><a href="sgq.php">Sistema de Gestão da Qualidade</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-heart" style="color: #2d4f6e;"></i>
              <h3><a href="visualiza_elogios.php">Visualizar Elogios</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-book" style="color: #2d4f6e;"></i>
              <h3><a href="http://portais.planosantacasasaude.santacasasjc.com.br:63499/formguiamedico/" target="_blank">Guia Médico</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-pencil-alt" style="color: #2d4f6e;"></i>
              <h3><a href="form_assinatura.php">Cadastrar Nova Assinatura</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-search" style="color: #2d4f6e;"></i>
              <h3><a href="http://portais.planosantacasasaude.santacasasjc.com.br:60080/procedimento/index" target="_blank">Tabela de Racionalização</a></h3>
            </div>
          </div>

          <?php if ($setor == "TI") { 
            echo '<div class="col-lg-3 col-md-4 mt-4">
              <div class="icon-box">
                <i class="bi bi-file-plus" style="color: #2d4f6e;"></i>
                <h3><a href="formComunicados.php">Cadastrar comunicado Interno</a></h3>
              </div>
            </div>';
          }
          ?>
          
          <?php if ($setor == "TI" || $setor == "OUVIDORIA") { 
            echo '<div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-file-richtext" style="color: #2d4f6e;"></i>
              <h3><a href="formFaq.php">Cadastrar FAQ</a></h3>
            </div>
          </div>';}?>

          <?php if ($setor == "TI" || $setor == "OUVIDORIA") { 
            echo '<div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-hand-holding-heart" style="color: #2d4f6e;"></i>
              <h3><a href="elogio.php">Cadastrar Elogios</a></h3>
            </div>
          </div>';}?>
          
          <?php if ($setor == "TI" || $setor == "OUVIDORIA") { 
            echo '
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-user-plus" style="color: #2d4f6e;"></i>
              <h3><a href="registro_func.php">Cadastrar Novo Colaborador</a></h3>
            </div>
          </div>';}?>
          
          <?php if ($setor == "TI" || $setor == "OUVIDORIA") { 
            echo '
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-journal-plus" style="color: #2d4f6e;"></i>
              <h3><a href="formulario_gestao_quali.php">Insersão de arquivos Qualidade</a></h3>
            </div>
          </div>';}?>
        </div>
      </div>
    </section>

    <?php include 'contato.php'; 
    include 'footer.php'; 
    ?>
    
  </main>
  <style>
  .carousel-control.left {
    background-image: -webkit-linear-gradient(left,rgb(0 0 0 / 0%) 0,rgba(0,0,0,.0001) 100%) !important;
  }
  .carousel-control.right {
    background-image: -webkit-linear-gradient(left,rgb(0 0 0 / 0%) 0,rgba(0,0,0,.0001) 100%) !important;
  }
  .carousel-control .glyphicon-chevron-left, .carousel-control .icon-prev {
    margin-left: -130px;
    color: #105794;
  }
  .carousel-control .glyphicon-chevron-right, .carousel-control .icon-next {
      margin-right: -130px;
      color: #105794;
  }
  </style>
</body>
</html>


<script>
  document.onmousedown=disableclick;
  function disableclick(event)
  {
    if(event.button==2)
    {
      return false;    
    }
  }
</script>