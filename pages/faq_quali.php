<?php include 'head.html'?>

<script src="../assets/js/modal.js"></script>

<head>
  <link rel="stylesheet" href="../assets/css/modal.css">
</head>

<body>
  <?php include 'header.php';
  include 'login/verifySessionStarted.php';

  $sql = "SELECT * FROM info_users WHERE CD_USUARIO LIKE UPPER('".$_SESSION['usuario']."')";
  $stid = oci_parse($ora_conexao, $sql) or die ("erro");
  oci_execute($stid);

  while (oci_fetch($stid)) {
    $_SESSION['setor'] = oci_result($stid, "LOTACAO");
  }
  ?>
  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="faq.php">FAQ - Qualidade</a></li>
          </ol>
        </div>
      </div>
    </section>

    <section id="faq" class="faq">
      <div class="section-title aos-init aos-animate" data-aos="fade-up"><h2>FAQ - Perguntas frequentes</h2></div>
      <div class="container">
        <div class="section-title" data-aos="fade-up"></div>
        <div class="div-space">
          <form name="registar" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
            <input class="button" name="submit" type="submit" value="Buscar" />
            <div class="input"><input type="text" name="oculto" value=""></div>
          </form>
        </div>
        <?php 
        if (!isset($_POST['submit']) or strlen($_POST['oculto']) == 0) {
          $sql = "SELECT extract(month from DATA_CRIACAO) AS mes, 
          extract(day from DATA_CRIACAO) AS DAY,
          extract(year from DATA_CRIACAO) AS ano,
          extract(month from DATA_EDICAO) AS mes_edicao,
          extract(day from DATA_EDICAO) AS DAY_edicao,
          extract(year from DATA_EDICAO) AS ano_edicao,
          COD, AUTHOR, TITLE, CATEGORY, CONTENT, DATA_EDICAO, FILE_LOCAL  FROM faq  WHERE CATEGORY LIKE 'Qualidade' ORDER BY cod ASC";
          $stid = oci_parse($ora_conexao, $sql) or die ("erro");
          oci_execute($stid); 

          while (oci_fetch($stid)) {
            $file = explode('/', oci_result($stid, "FILE_LOCAL"));
            $extencao =  $file[6][-4].$file[6][-3].$file[6][-2].$file[6][-1];
            
            // Formato de Data de Criação            
            if(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") < 9){
              $data = "0".oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
            }elseif(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") > 9){
              $data = oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
            }elseif(oci_result($stid, "MES") > 9 and oci_result($stid, "DAY") < 9){
              $data = "0".oci_result($stid, "DAY")."/".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
            }else{
              $data = oci_result($stid, "DAY")."/".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
            }

            // Formato de Data de Edição
            if(oci_result($stid, "MES_EDICAO") < 9 and oci_result($stid, "DAY_EDICAO") < 9){
              $data_edicao = "0".oci_result($stid, "DAY_EDICAO")."/0".oci_result($stid, "MES_EDICAO")."/".oci_result($stid, "ANO_EDICAO");
            }elseif(oci_result($stid, "MES_EDICAO") < 9 and oci_result($stid, "DAY_EDICAO") > 9){
              $data_edicao = oci_result($stid, "DAY_EDICAO")."/0".oci_result($stid, "MES_EDICAO")."/".oci_result($stid, "ANO_EDICAO");
            }elseif(oci_result($stid, "MES_EDICAO") > 9 and oci_result($stid, "DAY_EDICAO") < 9){
              $data_edicao = "0".oci_result($stid, "DAY_EDICAO")."/".oci_result($stid, "MES_EDICAO")."/".oci_result($stid, "ANO_EDICAO");
            }else{
              $data_edicao = oci_result($stid, "DAY_EDICAO")."/".oci_result($stid, "MES_EDICAO")."/".oci_result($stid, "ANO_EDICAO");
            }

            echo"
            <div class='accordion accordion-flush' id='accordionFlushExample'>
            <div class='accordion-item'>
            <h2 class='accordion-header' id='flush-heading".oci_result($stid, "COD")."'>
            
            <button class='accordion-button collapsed' type='button' data-bs-toggle='collapse' data-bs-target='#flush-collapse".oci_result($stid, "COD")."' aria-expanded='false' aria-controls='flush-collapse".oci_result($stid, "COD")."'>
            <strong>Autor: &nbsp</strong>".oci_result($stid, "AUTHOR")." -&nbsp
            <strong>Título: &nbsp</strong>".oci_result($stid, "TITLE")." &nbsp - &nbsp <strong>Categoria: &nbsp</strong>".oci_result($stid, "CATEGORY")."
            ";

            if($_SESSION['setor'] == '000103 TI' or $_SESSION['setor'] == '000134 RECURSOS HUMANOS'){
              echo " <a href='contact/deleta_faq.php?cod=".oci_result($stid, "COD")."' style='float: right; margin: 10px'>
              <i class='far fa-trash-alt'></i></a>
              <a href='edita_faq.php?cod=".oci_result($stid, "COD")."' style='float: right;'><i class='far fa-edit'></i></a>";
            }

            echo "
            </button>
            </h2>
            <div id='flush-collapse".oci_result($stid, "COD")."' class='accordion-collapse collapse' aria-labelledby='flush-heading".oci_result($stid, "COD")."' data-bs-parent='#accordionFlushExample'>
            <div class='accordion-body'>

            <div class='alert alert-primary' role='alert'>
            <strong>Conteudo:</strong> ".oci_result($stid, "CONTENT")."<br>
            <strong>Data de Criação: </strong>".$data."<br> <strong>Data de Edição: </strong>".$data_edicao."
            </div>";

            if ($extencao == '.jpg' or $extencao == 'jpeg' or $extencao == '.png') {
             echo "<a href='../".$file[4]."/".$file[5]."/".$file[6]."' target='_blank'>
             <img src='../".$file[4]."/".$file[5]."/".$file[6]."'  style='width:25%' class='hover-shadow cursor'>
             </a>";
           }else{
            echo "<a href='../".$file[4]."/".$file[5]."/".$file[6]."' target='_blank'>
            Visualizar Arquivo
            </a>";
          }
          
          echo "
          </div>
          </div>
          </div>
          ";
        }

      }elseif (isset($_POST['submit'])) {
        $sql = "SELECT extract(month from DATA_CRIACAO) AS mes, 
        extract(day from DATA_CRIACAO) AS DAY,
        extract(year from DATA_CRIACAO) AS ano,
        extract(month from DATA_EDICAO) AS mes_edicao,
        extract(day from DATA_EDICAO) AS DAY_edicao,
        extract(year from DATA_EDICAO) AS ano_edicao,
        COD, AUTHOR, TITLE, CATEGORY, CONTENT, DATA_EDICAO, FILE_LOCAL 
        FROM faq WHERE AUTHOR like '%".$_POST['oculto']."%' or TITLE like '%".$_POST['oculto']."%' OR CATEGORY like '%".$_POST['oculto']."%' OR CONTENT like '%".$_POST['oculto']."%'";
        
        $stid = oci_parse($ora_conexao, $sql) or die ("erro");
        oci_execute($stid); 


        while (oci_fetch($stid)) {
          $file = explode('/', oci_result($stid, "FILE_LOCAL"));
          $extencao =  $file[6][-4].$file[6][-3].$file[6][-2].$file[6][-1];
          
          // Formato de Data de Criação
          if(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") < 9){
            $data = "0".oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
          }elseif(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") > 9){
            $data = oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
          }elseif(oci_result($stid, "MES") > 9 and oci_result($stid, "DAY") < 9){
            $data = "/0".oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
          }else{
            $data = oci_result($stid, "DAY")."/".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
          }

          // Formato de Data de Edição
          if(oci_result($stid, "MES_EDICAO") < 9 and oci_result($stid, "DAY_EDICAO") < 9){
            $data_edicao = "0".oci_result($stid, "DAY_EDICAO")."/0".oci_result($stid, "MES_EDICAO")."/".oci_result($stid, "ANO_EDICAO");
          }elseif(oci_result($stid, "MES_EDICAO") < 9 and oci_result($stid, "DAY_EDICAO") > 9){
            $data_edicao = oci_result($stid, "DAY_EDICAO")."/0".oci_result($stid, "MES_EDICAO")."/".oci_result($stid, "ANO_EDICAO");
          }elseif(oci_result($stid, "MES_EDICAO") > 9 and oci_result($stid, "DAY_EDICAO") < 9){
            $data_edicao = "0".oci_result($stid, "DAY_EDICAO")."/".oci_result($stid, "MES_EDICAO")."/".oci_result($stid, "ANO_EDICAO");
          }else{
            $data_edicao = oci_result($stid, "DAY_EDICAO")."/".oci_result($stid, "MES_EDICAO")."/".oci_result($stid, "ANO_EDICAO");
          }

          echo"
          <div class='accordion accordion-flush' id='accordionFlushExample'>
          <div class='accordion-item'>
          <h2 class='accordion-header' id='flush-heading".oci_result($stid, "COD")."'>
          
          <div class='container'>
          <button class='accordion-button collapsed' type='button' data-bs-toggle='collapse' data-bs-target='#flush-collapse".oci_result($stid, "COD")."' aria-expanded='false' aria-controls='flush-collapse".oci_result($stid, "COD")."'>
          <strong>Autor: &nbsp</strong>".oci_result($stid, "AUTHOR")." -&nbsp
          <strong>Título: &nbsp</strong>".oci_result($stid, "TITLE")." &nbsp - &nbsp <strong>Categoria: &nbsp</strong>".oci_result($stid, "CATEGORY")."
          ";

          if($_SESSION['setor'] == '000103 TI' or $_SESSION['setor'] == '000134 RECURSOS HUMANOS'){
            echo " <a href='contact/deleta_faq.php?cod=".oci_result($stid, "COD")."' style='float: right; margin: 10px'>
            <i class='far fa-trash-alt'></i></a>
            <a href='edita_faq_quali.php?cod=".oci_result($stid, "COD")."' style='float: right;'><i class='far fa-edit'></i></a>";
          }

          echo "
          </button></div>
          </h2>
          <div id='flush-collapse".oci_result($stid, "COD")."' class='accordion-collapse collapse' aria-labelledby='flush-heading".oci_result($stid, "COD")."' data-bs-parent='#accordionFlushExample'>
          <div class='accordion-body'>

          <div class='alert alert-primary' role='alert'>
          <strong>Conteudo:</strong> ".oci_result($stid, "CONTENT")."<br>
          <strong>Data de Criação: </strong>".$data."<br> <strong>Data de Edição: </strong>".$data_edicao."
          </div>";

          if ($extencao == '.jpg' or $extencao == 'jpeg' or $extencao == '.png') {
           echo "<a href='../".$file[4]."/".$file[5]."/".$file[6]."' target='_blank'>
           <img src='../".$file[4]."/".$file[5]."/".$file[6]."'  style='width:25%' class='hover-shadow cursor'>
           </a>";
         }else{
          echo "<a href='../".$file[4]."/".$file[5]."/".$file[6]."' target='_blank'>
          Visualizar Arquivo
          </a>";
        }
        
        echo "
        </div>
        </div>
        </div>
        ";
      }       
    } 
    ?>      
  </div>
</section>
</main>
</div>
<div style="padding-top: 5%">
  <?php include 'footer.php' ?>
</div>

</body>
</html>


