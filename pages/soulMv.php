<?php include '../actions/conecta.php';?>

<!DOCTYPE html>
<html lang="en">
<?php include 'head.html'?>

<body>

  <?php include 'header.php';?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="soulMv.php">Página de Acesso ao MV</a></li>
          </ol>
        </div>
      </div>
    </section>
    <section id="features" class="features"  style="padding-bottom: 100px">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Ambiente de Produção</h2>
        </div>
        <div class="row" data-aos="fade-up" data-aos-delay="300">

          <div class="col-lg-3 col-md-4">
            <div class="icon-box">
              <h3><a href="http://10.201.0.20/">Soul MV</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <h3><a href="http://10.201.0.20/plano">MV Contingência</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <h3><a href="http://10.200.0.29/mv">MV Hospitalar</a></h3>
            </div>
          </div>
        
        <div class="col-lg-3 col-md-4 mt-4 mt-md-0 dropdown" >
            <div class="icon-box"><h3>Portais MV</h3></div>
            <div class="dropdown-content">
                <a href="http://10.201.0.20/mvsaudeweb/#/login/beneficiario">Beneficiario</a>
                <a href="http://10.201.0.20/mvsaudeweb/#/login/empresa">Empresa</a>
                <a href="http://10.201.0.20/mvsaudeweb/#/login/corretor">Corretor</a>
                <a href="http://10.201.0.20/mvsaudeweb/#/login/corretor">Faturamento XML</a>
                <a href="http://10.201.0.20/mvautorizadorguias/">Autorizador Web</a>
            </div>
        </div>
          
        </div>
      </div>
    </section>

    <section class="features">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Ambiente de Treinamento</h2>
        </div>
        <div class="row" data-aos="fade-up" data-aos-delay="300">

          <div class="col-lg-3 col-md-4">
            <div class="icon-box">
              <h3><a href="http://10.201.0.243:82/">Soul MV</a></h3>
            </div>
          </div>
        
        <div class="col-lg-3 col-md-4 mt-4 mt-md-0 dropdown">
            <div class="icon-box"><h3>Portais MV</h3></div>
            <div class="dropdown-content">
                <a href="http://10.201.0.243:82/mvsaudeweb/#/login/beneficiario">Beneficiario</a>
                <a href="http://10.201.0.243:82/mvsaudeweb/#/login/empresa">Empresa</a>
                <a href="http://10.201.0.243:82/mvsaudeweb/#/login/corretor">Corretor</a>
                <a href="http://10.201.0.243:82/mvsaudeweb/#/login/prestador">Faturamento XML</a>
                <a href="http://10.201.0.243:82/mvautorizadorguias/">Autorizador Web</a>
            </div>
        </div>
          
        </div>
      </div>
    </section>
  </main>

  <div style="padding-top: 7%">
    <?php include 'footer.php' ?>
  </div>

</body>

<style>
    .dropdown {
  display: inline-block;
  position: relative;
}
.dropdown-content {
  display: none;
  position: absolute;
  width: 100%;
  box-shadow: 0px 10px 10px 0px rgba(0,0,0,0.4);
}
.dropdown:hover .dropdown-content {
  display: block;
}
.dropdown-content a {
  display: block;
  color: #000000;
  padding: 5px;
  text-decoration: none;
}
.dropdown-content a:hover {
  color: #FFFFFF;
  background-color: #00A4BD;
}
</style>

</html>