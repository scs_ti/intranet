<?php include 'head.html'?>
<body>
  <?php include 'header.php';
  include 'login/verifySessionStarted.php';
  include 'login/lvl_access.php';
  date_default_timezone_set('UTC');
  ?>
  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="formComunicados.php">Registrar Comunicado</a></li>
          </ol>
        </div>
      </div>
    </section>
    <section id="faq" class="faq">
      <div class="section-title aos-init aos-animate" data-aos="fade-up">
        <h2>Cadastrar novo Comunicado</h2>
      </div>
      <div class="container">
        <div class="section-title" data-aos="fade-up"></div>          
        <form action="contact/contact_comunicados.php" method="POST" enctype="multipart/form-data">
          <div class="form-group">
            <label>Autor:</label>
            <input type="text" name="autor" class="form-control" readonly="readonly" id="autor" value="<?php echo($_SESSION['usuario']);?>"><br>
          </div>
          <div class="form-group">
            <label>Titulo:</label>
            <input type="text" name="titulo" class="form-control" required="required" id="titulo" value=""><br>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">Data de Criação</label>
            <input type="date" name="data_criacao" class="form-control" required="required" id="data_criacao" value= "">
          </div><br>
          <div class="form-group">
            <label for="conteudo">Conteúdo</label>
            <textarea class="form-control" name="conteudo" id="conteudo" rows="3" maxlength="255" required="required"></textarea>
          </div><br>
          <div class="form-group">
            <label for="conteudo">Enviar Arquivo</label>
            <input class="form-control" type="file" name="fileToUpload" id="fileToUpload">
          </div><br>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Salvar</button>
            <button type="button"  class="btn btn-danger"><a href="index.php" style="color: #fff">Cancelar</a></button>
          </div>
        </form>
      </div>
    </section>
  </section>
</main>
<div style="padding-top: 6%">
  <?php include 'footer.php' ?>
</div>
</body>
</html>

