<?php include 'head.html'?>

<body>
  <?php include 'header.php';
  //include 'login/verifySessionStarted.php';
  ?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="RHMaster.php">RH Master</a></li>
          </ol>
        </div>
      </div>
    </section>

    <section id="faq" class="faq">
      <div class="section-title aos-init aos-animate" data-aos="fade-up">
        <h2>RH Master</h2>
        <p>Para que esta página funcione corretamente, por favor, instale a extensão <strong>
          <a href="https://chrome.google.com/webstore/detail/ignore-x-frame-headers/gleekbfjekiniecknbkamfmkohkpodhe" target="_blank">Ignore X-Frame headers</a></strong> no seu navegador.</p>
      </div>
      <div class="container">
        <div class="section-title" data-aos="fade-up"></div>
        <iframe src="https://rhmaster-prime.com.br/rhmaster.prime-2.0/home.jsf" width="100%" height="600px" frameborder="0" marginheight="0" marginwidth="0" scrolling="no"></iframe>
      </div>
    </section>
  </main>
</div>

<?php include 'footer.php' ?>

</body>
</html>