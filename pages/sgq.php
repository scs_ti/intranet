<?php include '../actions/conecta.php';?>

<!DOCTYPE html>
<html lang="en">
<?php include 'head.html'?>

<body>

  <?php include 'header.php';
  include 'login/verifySessionStarted.php';
  ?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="sgq.php">Sistema de Gestão da Qualidade</a></li>
          </ol>
        </div>
      </div>
    </section>
    <section id="features" class="features">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Sistema de Gestão da Qualidade</h2>
        </div>
        <div class="row" data-aos="fade-up" data-aos-delay="300">

          <div class="col-lg-4 col-md-4">
            <div class="icon-box">
              <h3><a href="./sgq/setores.php?tipo=1">Políticas Institucionais</a></h3>
            </div>
          </div>

          <div class="col-lg-4 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <h3><a href="./sgq/setores.php?tipo=2">POP</a></h3>
            </div>
          </div>

          <div class="col-lg-4 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <h3><a href="./sgq/setores.php?tipo=3">Manual</a></h3>
            </div>
          </div>

          <div class="col-lg-4 col-md-4 mt-4">
            <div class="icon-box">
              <h3><a href="./sgq/setores.php?tipo=4">SIPOC</a></h3>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-4 mt-4">
            <div class="icon-box">
              <h3><a href="./sgq/setores.php?tipo=5">Fluxogramas</a></h3>
            </div>
          </div>

          <div class="col-lg-4 col-md-4 mt-4">
            <div class="icon-box">
              <h3><a href="./sgq/setores.php?tipo=6">Formulários para Download</a></h3>
            </div>
          </div>

          <div class="col-lg-4 col-md-4 mt-4">
            <div class="icon-box">
              <h3><a href="./sgq/setores.php?tipo=7">Treinamento da Qualidade</a></h3>
            </div>
          </div>

          <div class="col-lg-4 col-md-4 mt-4">
            <div class="icon-box">
              <h3><a href="./sgq/setores.php?tipo=8">Formulários</a></h3>
            </div>
          </div>

        </div>
      </div>
    </section>
  </main>

  <div style="padding-top: 14%">
    <?php include 'footer.php' ?>
  </div>

</body>

</html>