﻿<?php include 'head.html'?>

<body>
  <?php include 'header.php';
  //include 'login/verifySessionStarted.php';
  ?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="usuarios.php">Colaboradores</a></li>
          </ol>
        </div>
      </div>
    </section>

    <section id="team" class="team section-bg">
    <div class="section-title aos-init aos-animate" data-aos="fade-up">
        <h2>Colaboradores</h2>
      </div>
      <div class="container">
        <div class="section-title" data-aos="fade-up"></div>
          <div class="div-space">
            <form name="registar" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
              <input class="button" name="submit" type="submit" value="Buscar" />
              <div class="input"><input type="text" name="oculto" value=""></div>
            </form>
          </div>
          <!-- <div data-aos="fade-up">
            <?php include 'lista_departamento.php' ?>
          </div> -->
          <div class="row">
          <?php 
          if (!isset($_POST['submit']) or strlen($_POST['oculto']) == 0) {
            $sql = "SELECT * FROM info_users ORDER BY COLABORADOR";
            $stid = oci_parse($ora_conexao, $sql) or die ("erro");
            oci_execute($stid); 
            while (oci_fetch($stid)) {
              $foto = "../files/usuarios/".oci_result($stid, 'CD_USUARIO').".jpg";
              $file = explode('@', oci_result($stid, "DS_EMAIL"));
              echo"
              <div class='col-lg-4 align-items-stretch'>
              <div class='member' data-aos='fade-up' data-aos-delay='100'>
              <div class='member-img'>"?>
              <?php 

              $var = oci_result($stid, "LOTACAO");
              $setor = (string)$var; 
              $setor = explode(" ", (string)$setor);

              if(file_exists($foto)){
                echo "<img src='".$foto."' class='img-fluid' style='max-width: 200px;max-height: 200px;padding-top: 15px;border-radius: 50%;' alt=''>";
              }else{
                echo "<img src='https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png' class='img-fluid' style='max-width: 200px;max-height: 200px;' alt=''>";
              }
              ?>
              <?php echo "</div>
              <div class='member-info'>
              <h4>".ucwords(oci_result($stid, "COLABORADOR"))."</h4>
              <span>Setor: ".strtolower($setor[1])."</span>
              <span>Email: ".strtolower(oci_result($stid, "DS_EMAIL"))."</span>
              <span>Ramal: ".oci_result($stid, "RAMAL")."</span>
              </div>
              </div>
              </div>";
            }

          }elseif (isset($_POST['submit'])) {
            $sql =  "SELECT * FROM info_users 
            WHERE COLABORADOR 
            like UPPER('".$_POST['oculto']."%') or
            COLABORADOR like UPPER('".$_POST['oculto']."%') 
            or LOTACAO like UPPER('%".$_POST['oculto']."%') 
            or DS_EMAIL like UPPER('".$_POST['oculto']."%')
            or CD_USUARIO like UPPER('".$_POST['oculto']."%')
            ORDER BY COLABORADOR";
            
            $stid = oci_parse($ora_conexao, $sql) or die ("erro");
            oci_execute($stid); 

            while (oci_fetch($stid)) {
              $foto = "../files/usuarios/".oci_result($stid, 'CD_USUARIO').".jpg";
              $file = explode('@', oci_result($stid, "DS_EMAIL"));
              echo"
              <div class='col-lg-4 align-items-stretch'>
              <br>
              <div class='member' data-aos='fade-up' data-aos-delay='100'>
              <div class='member-img'>"?>
              <?php 
              if(file_exists($foto)){
                echo "<img src='".$foto."' class='img-fluid' style='max-width: 200px;max-height: 200px;padding-top: 15px;border-radius: 50%;' alt=''>";
              }else{
                echo "<img src='https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png' class='img-fluid' style='max-width: 200px;max-height: 200px; padding-top: 15px;border-radius: 50%;' alt=''>";
              }
              ?>
              <?php 
              
              $var = oci_result($stid, "LOTACAO");
              $setor = (string)$var; 
              $setor = explode(" ", (string)$setor);

              echo "</div>
              <div class='member-info'>
              <h4>".ucwords(oci_result($stid, "COLABORADOR"))."</h4>
              <span>Setor: ".strtolower($setor[1])."</span>
              <span>Email: ".strtolower(oci_result($stid, "DS_EMAIL"))."</span>
              <span>Ramal: ".oci_result($stid, "RAMAL")."</span>
              </div>
              </div>
              </div>";
            }
          }
          ?>
      </section>
    </main>
  </div>
  <?php include 'footer.php' ?>
</body>
</html>