<?php include 'head.html';
ini_set('default_charset', 'UTF-8');
?>

<body>

  <?php include 'header.php';
  include 'login/verifySessionStarted.php';
  // include 'login/lvl_access_TI.php';
  ?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="formFaq.php">Registrar FAQ</a></li>
          </ol>
        </div>
      </div>
    </section>
    <section id="faq" class="faq">
      <div class="section-title aos-init aos-animate" data-aos="fade-up">
        <h2>Cadastrar novo FAQ</h2>
      </div>
      <div class="container">
        <div class="section-title" data-aos="fade-up"></div>
        <form action="contact/contact_faq.php" method="POST" enctype="multipart/form-data">
          <div class="form-group">
            <label>Autor:</label>
            <input type="text" name="autor" class="form-control" readonly="readonly" id="autor" value="<?php echo($_SESSION['usuario']);?>"><br>
          </div>
          <div class="form-group">
            <label>Titulo:</label>
            <input type="text" name="titulo" class="form-control" required="required" id="titulo" value=""><br>
          </div>
          <div class="form-group">
            <label>Data de Criação:</label>
            <input type="date" name="data" class="form-control" required="required" id="data" value=""><br>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect1">Categoria</label>
            <select name="categoria" class="form-control" id="exampleFormControlSelect1">
              <option>000101 JURIDICO</option>
              <option>000103 TI</option>
              <option>000104 ATENDIMENTO/EMISSAO.GUIAS</option>
              <option>000106 TELEFONISTAS</option>
              <option>000108 CADASTRO</option>
              <option>000109 LIMPEZA</option>
              <option>000110 COMERCIAL</option>
              <option>000112 CONTABILIDADE</option>
              <option>000113 AUDITORIA</option>
              <option>000115 CONTAS MEDICAS</option>
              <option>000116 CREDENCIAMENTO</option>
              <option>000118 SAUDE PREVENTIVA</option>
              <option>000119 COBRANCA</option>
              <option>000121 FATURAMENTO</option>
              <option>000122 FINANCEIRO</option>
              <option>000124 OUVIDORIA</option>
              <option>000125 REGULACAO</option>
              <option>000127 ADM TAUBATE</option>
              <option>000128 CARAGUATATUBA</option>
              <option>000129 SAO SEBASTIAO</option>
              <option>000130 JACAREI</option>
              <option>000132 PINDAMONHANGABA</option>
              <option>000133 CRUZEIRO</option>
              <option>000134 RECURSOS HUMANOS</option>
              <option>000135 GUARATINGUETA</option>
              <option>00137 RECURSO DE GLOSA</option>
              <option>00139 CACHOEIRA PAULISTA</option>
              <option>00141 SECRETARIA</option>
              <option>00142 RELACIONAMENTO COM CLIENTE</option>
              <option>00143 SUPRIMENTOS</option>
              <option>00144 PARAMETRIZAÇÃO E ADEQUAÇÃO</option>
            </select>
          </div><br>
          <div class="form-group">
            <label for="conteudo">Conteúdo</label>
            <textarea class="form-control" name="conteudo" id="conteudo" rows="3" maxlength="255" required="required"></textarea>
          </div><br>
          <div class="form-group">
            <label for="conteudo">Enviar Arquivo</label>
            <input class="form-control" type="file" name="fileToUpload" id="fileToUpload">
          </div><br>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Salvar</button>
            <button type="button"  class="btn btn-danger"><a href="index.php" style="color: #fff">Cancelar</a></button>
          </div>
        </form>
      </div>
    </section>
  </section>
</main>
</div>
<?php include 'footer.php' ?>
</body>
</html>