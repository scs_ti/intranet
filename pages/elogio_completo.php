<?php include 'head.html'?>

<script src="../assets/js/modal.js"></script>

<head>
  <link rel="stylesheet" href="../assets/css/modal.css">
  <link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>
  <?php include 'header.php';
  include 'login/verifySessionStarted.php';

  $consulta_elogio = "SELECT * FROM DBAPS.elogio";
  $stid_elogio = oci_parse($ora_conexao, $consulta_elogio) or die ("erro");
  oci_execute($stid_elogio);

  ?>
  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="faq.php">Elogios</a></li>
          </ol>
        </div>
      </div>
  </section>

  <section id="faq" class="faq">
    <div class="section-title aos-init aos-animate" data-aos="fade-up">
      <h2>Elogio</h2>
    </div>
    <div class="container">
      <div class="card">
        <div class="card-body">
          <form>
            <?php 
              $consulta = "SELECT * FROM DBAPS.elogio WHERE ID_ELOGIO = ".$_GET['id']."";
              $stid_elogio = oci_parse($ora_conexao, $consulta) or die ("erro");
              oci_execute($stid_elogio); 
              while (oci_fetch($stid_elogio)) {
                $dia = oci_result($stid_elogio, 'DATA_ELOGIO');
                echo '
                <div class="form-row">
                  <div class="form-group col-md-12">
                    <label for="inputEmail4">Autor</label>
                    <input type="email" class="form-control" readonly="readonly" value="'.oci_result($stid_elogio, 'AUTOR').'" id="inputEmail4" placeholder="Email">
                  </div><br>
                  <div class="form-group col-md-12">
                    <label for="inputPassword4">Data do Elogio</label>
                    <input type="text" class="form-control" value = "'.date('d/m/Y', strtotime($dia)).'" readonly="readonly" id="inputPassword4" placeholder="Senha">
                  </div>
                </div><br>
                <div class="form-group">
                  <label for="inputAddress">Canal de Entrada</label>
                  <input type="text" class="form-control" value = '.oci_result($stid_elogio, 'CANAL_ENTRADA').' readonly="readonly" id="inputAddress" placeholder="Rua dos Bobos, nº 0">
                </div><br>
                <div class="form-group">
                  <label for="inputAddress">Setor Elogiado</label>
                  <input type="text" class="form-control" value = '.oci_result($stid_elogio, 'SETOR_ELOGIO').' readonly="readonly" id="inputAddress" placeholder="Rua dos Bobos, nº 0">
                </div><br>
                <div class="form-group">
                  <label for="inputAddress">Elogio Direcionado</label>
                  <input type="text" class="form-control" value = '.oci_result($stid_elogio, 'ELOGIO_DIRECIONADO').' readonly="readonly" id="inputAddress" placeholder="Rua dos Bobos, nº 0">
                </div><br>
                <div class="form-group">
                  <label for="inputAddress">Elogio</label>
                 <textarea rows="4" cols="50" class="form-control" readonly="readonly" id="inputAddress">'.oci_result($stid_elogio, 'CONTEUDO_ELOGIO').'</textarea>
                </div><br>
                <button type="button" class="btn btn-primary"><a href="visualiza_elogios.php" style="color: #fff">Voltar</a></button>
                ';}
            ?>
          </form>
        </div>
      </div>
    </div>
  </section>
</main>
</div>
<div style="padding-top: 5%">
  <?php include 'footer.php' ?>
</div>
</body>
</html>



  