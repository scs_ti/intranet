<?php include 'head.html'?>

<body>

  <?php include 'header.php';
  include 'login/verifySessionStarted.php';
  ?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="perfil.php">Perfil</a></li>
          </ol>
        </div>
      </div>
    </section>

    <section id="team" class="team section-bg">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Meu Perfil</h2>
        </div>
        
        <center>
          <form enctype="multipart/form-data" action="contact/cadastro_foto.php" method="POST">
            <input type="hidden" name="MAX_FILE_SIZE" value="250000" />
            Enviar esse arquivo: <input name="userfile" type="file" />
            <input type="submit" value="Enviar arquivo" />
          </form>
        </center>

        <div class="row">
          <form action="contact/contact_profile.php" method="POST">
            <?php 
            if (!isset($_POST['submit']) or strlen($_POST['oculto']) == 0) {
              $sql = "SELECT extract(month from DT_NASCIMENTO) AS mes, extract(day from DT_NASCIMENTO) AS DAY, extract(year from DT_NASCIMENTO) AS ano, COLABORADOR, LOTACAO, RAMAL, CD_USUARIO, DS_EMAIL FROM info_users WHERE CD_USUARIO LIKE UPPER('".$_SESSION['usuario']."')";
              $stid = oci_parse($ora_conexao, $sql) or die ("erro");
              oci_execute($stid); 

              while (oci_fetch($stid)) {
                $foto = "../files/usuarios/".oci_result($stid, 'CD_USUARIO').".jpg";?>
                <?php if(file_exists($foto)){
                  echo "<center><img src='".$foto."' class='img-fluid' style='max-width: 10%; min-height: 5%; padding-top: 15px; border-radius: 50%;' alt=''></center><br>
                    <div class='alert alert-danger' role='alert'>
                    <center><strong>Alerta</strong>: Todas as fotos são verificadas pela equipe de RH, por favor, usar foto adequada.</center>
                    </div>";
                }else{
                  echo "<center><img src='https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png' class='img-fluid' style='max-width: 10%;' alt=''></center>";
                }
                ?>
                <?php

                if(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") < 9){
                  $data = oci_result($stid, "ANO")."-0".oci_result($stid, "MES")."-0".oci_result($stid, "DAY");
                }elseif(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") > 9){
                  $data = oci_result($stid, "ANO")."-0".oci_result($stid, "MES")."-".oci_result($stid, "DAY");
                }elseif(oci_result($stid, "MES") > 9 and oci_result($stid, "DAY") < 9){
                  $data = oci_result($stid, "ANO")."-0".oci_result($stid, "MES")."-0".oci_result($stid, "DAY");
                }else{
                  $data = oci_result($stid, "ANO")."-".oci_result($stid, "MES")."-".oci_result($stid, "DAY");
                }
                
                $var = oci_result($stid, "LOTACAO");
                $setor = (string)$var; 
                $setor = explode(" ", (string)$setor);
                
                echo "<div class='form-group'>
                <label>Usuário:</label>
                <input type='text' name='CD_USUARIO' class='form-control' readonly='readonly' id='CD_USUARIO' value='".oci_result($stid, "CD_USUARIO")."'><br>
                </div>
                <div class='form-group'>
                <label>Nome:</label>
                <input type='text' name='NM_USUARIO' class='form-control' readonly='readonly' id='NM_USUARIO' value='".oci_result($stid, "COLABORADOR")."'><br>
                </div>
                <div class='form-group'>
                <label>Setor:</label>
                <input type='text' name='lotacao' class='form-control' readonly='readonly' id='DS_DEPTO_OPERADORA' value='".$setor[1]."'><br>
                </div>
                <div class='form-group'>
                <label>Ramal:</label>
                <input type='text' name='RAMAL' class='form-control' id='RAMAL' value='".oci_result($stid, "RAMAL")."'><br>
                </div>
                <div class='form-group'>
                <label>Data de Nascimento: </label>
                <input name='DT_NASCIMENTO' type='date' class='form-control' id='DT_NASCIMENTO' value='".$data."'><br>
                </div>
                <div class='form-group'>
                <label>Email:</label>
                <input type='text' name='DS_EMAIL' class='form-control' id='DS_EMAIL' value='".strtolower(oci_result($stid, "DS_EMAIL"))."'><br>
                </div>
                ";
              }
            }
            ?>  
            <button type="submit" class="btn btn-success">Salvar</button>
            <button type="button"  class="btn btn-danger"><a href="index.php" style="color: #fff">Cancelar</a></button>
          </form>
        </div>
      </section>
    </main>
  </div>

  <?php include 'footer.php' ?>

</body>
</html>