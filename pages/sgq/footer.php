 <footer id="footer">
  <div class="container">
    <div class="row d-flex align-items-center">
      <div class="col-lg-6 text-lg-left text-center">
        <div class="copyright">
          &copy; Copyright <strong>Plano Santa Casa Saúde</strong>. Todos os direitos reservados
        </div>
        <div class="credits">
          Desenvolvido por <a href="">TI Plano Santa Casa Saúde</a>
        </div>
      </div>
      <div class="col-lg-6">
        <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
          <a href="index.php" class="scrollto">Início</a>
          <a href="faq.php" class="scrollto">FAQ</a>
          <a href="index.php#contact">Contato</a>
          <?php if (isset($_SESSION['usuario'])) {
            $stid = oci_parse($ora_conexao, "SELECT * FROM info_users where CD_USUARIO like UPPER('".$_SESSION['usuario']."')");
            oci_execute($stid); 
            while (oci_fetch($stid)) {
              $name = ucwords(strtolower(oci_result($stid, "COLABORADOR")));
              echo "<a href='../index.php' class='getstarted scrollto'>Bem-vindo(a) ".$name."</a>";
              echo('<a href="../login/logout.php">Sair</a>');
            }
          }else{
            echo('<a href="../login/login.php">Acessar intranet</a>');
          } ?>
        </nav>
        <center><img src="../../files/registro ANS.jfif" style="width: 35%;"></center>
      </div>
    </div>
  </div>
</footer>


<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="../../assets/vendor/aos/aos.js"></script>
<script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="../../assets/vendor/php-email-form/validate.js"></script>
<script src="../../assets/vendor/purecounter/purecounter.js"></script>
<script src="../../assets/vendor/swiper/swiper-bundle.min.js"></script>

<!-- Template Main JS File -->
<script src="../../assets/js/main.js"></script>