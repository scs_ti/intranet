

<?php include '../actions/conecta.php';?>

<!DOCTYPE html>
<html lang="en">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css" rel="stylesheet">
<?php include './head.html'?>

<body>

  <?php include './header.php';
  include '../login/verifySessionStarted.php';
  ?>

  <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="../index.php">Início</a></li>
            <li><a href="">Arquivos - Qualidade</a></li>
          </ol>
        </div>
      </div>
  </section>
  
  <br>

  <?php
    if ($_GET['tipo'] == 1){
        $tipo = "Politicas Institucionais";
    }elseif ($_GET['tipo'] == 2){
        $tipo = "POP";
    }elseif ($_GET['tipo'] == 3){
        $tipo = "Manual";
    }elseif ($_GET['tipo'] == 4){
        $tipo = "SIPOC";
    }elseif ($_GET['tipo'] == 5){
        $tipo = "Fluxogramas";
    }elseif ($_GET['tipo'] == 6){
        $tipo = "Formulários para Download";
    }elseif ($_GET['tipo'] == 7){
        $tipo = "Treinamento da Qualidade";
      }elseif ($_GET['tipo'] == 8){
        $tipo = "Formulários";
    }

    if ($_GET['setor'] == 1){
        $setor = "RH";
    }elseif ($_GET['setor'] == 2){
        $setor = "Financeiro";
    }elseif ($_GET['setor'] == 3){
        $setor = "Qualidade";
    }elseif ($_GET['setor'] == 4){
        $setor = "Ouvidoria";
    }elseif ($_GET['setor'] == 5){
        $setor = "Atendimento";
    }elseif ($_GET['setor'] == 6){
        $setor = "Relacionamento Empresarial";
    }elseif ($_GET['setor'] == 7){
        $setor = "Auditoria";
    }
  ?>
    <br>
  <div class="section-title aos-init aos-animate" data-aos="fade-up">
      <h2><?php echo $tipo .' - '. $setor?></h2>
  </div>

  <?php 
    $consulta_elogio = "SELECT * FROM DBAPS.ARQUIVOS_GESTAO_QUALI where SETOR = '".$setor."' and CATEGORIA = '".$tipo."'";
    $stid_elogio = oci_parse($ora_conexao, $consulta_elogio) or die ("erro");
    oci_execute($stid_elogio);
  ?>

  <div class="container">
  <div class="card p-3">
    <table class="table table-striped" id="minhaTabela">
      <thead>
        <tr>
          <th>Titulo</th>
          <th>Categoria</th>
          <th>Setor</th>
          <th>Visualizar</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          while (($row = oci_fetch_array($stid_elogio, OCI_BOTH)) != false) {
            echo "<tr>
                    <td>".
                      $row['TITULO'].
                    "</td>
                    <td>".
                      $row['CATEGORIA'].
                    "</td>
                    <td>".
                    $row['SETOR'].
                    "</td>
                    "
                ;
              if ($row['CATEGORIA'] == 'Formulários'){
                echo "<td>
                      <a href='../../".$row['LOCAL_ARQUIVO']."' download class='btn btn-primary'>Download</a>
                    </td>
                  </tr>";
              }else{
                echo "<td>
                      <a href='../../".$row['LOCAL_ARQUIVO']."' class='btn btn-primary'>Visualizar</a>
                    </td>
                  </tr>";
              }
          }
        ?>
      </tbody>
    </table>
  </div>
  </div>
  
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>

  <script>
  $(document).ready(function(){
      $('#minhaTabela').DataTable({
        	"language": {
                "lengthMenu": "Mostrando _MENU_ registros por página",
                "zeroRecords": "Nada encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhum registro disponível",
                "infoFiltered": "(filtrado de _MAX_ registros no total)"
            }
        });
  });
  </script>
  <div style="padding-top: 19%">
  	<?php include 'footer.php' ?>
  </div>
</body>
</html>

