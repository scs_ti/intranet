<?php include 'head.html'?>

<body>

  <?php include 'header.php';
  include 'login/verifySessionStarted.php';
  // include 'login/lvl_access.php';
  ?>
  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="faq.php">FAQ</a></li>
            <?php echo '<li><a href="edita_faq.php?cod='.$_GET['cod'].' ">Editar FAQ</a></li>'?>
          </ol>
        </div>
      </div>
    </section>

    <section id="team" class="team section-bg">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Editar FAQ</h2>
        </div>

        <div class="row">
          <div class="container">
            <div class="section-title" data-aos="fade-up"></div>
            <?php

            $sql = "SELECT * FROM FAQ WHERE COD = ".$_GET['cod']." ";
            $stid = oci_parse($ora_conexao, $sql) or die ("erro");
            oci_execute($stid); 

            $dia = date("d");
            $mes = date("m");
            $ano = date("y");

            while (oci_fetch($stid)) {
              echo "
              <form action='contact/update_faq.php' method='POST' enctype='multipart/form-data'>
              <div class='form-group'>
              <label>Autor:</label>
              <input type='hidden' name='cod' class='form-control' id='cod' value='".oci_result($stid, "COD")."'>
              <input type='text' name='autor' class='form-control' id='autor' value='".oci_result($stid, "AUTHOR")."'><br>
              </div>
              <div class='form-group'>
              <label>Titulo:</label>
              <input type='text' name='titulo' class='form-control' required='required' id='titulo' value='".oci_result($stid, "TITLE")."'><br>
              </div>
              <div class='form-group'>
              <label>Data de Edição:</label>
              <input type='hidden' name='data_criacao' class='form-control' required='required' id='data' 
              value='".oci_result($stid, "DATA_CRIACAO")."'>
              <input type='text' name='data_edicao' readonly='readonly' class='form-control' required='required' id='data' value='".$dia."/".$mes."/20".$ano."'><br>
              </div>
              <div class='form-group'>
              <label for='exampleFormControlSelect1'>Categoria</label>
              <select name='categoria' class='form-control' id='exampleFormControlSelect1'>
              <option>000101 JURIDICO</option>
              <option>000103 TI</option>
              <option>000104 ATENDIMENTO/EMISSAO.GUIAS</option>
              <option>000106 TELEFONISTAS</option>
              <option>000108 CADASTRO</option>
              <option>000109 LIMPEZA</option>
              <option>000110 COMERCIAL</option>
              <option>000112 CONTABILIDADE</option>
              <option>000113 AUDITORIA</option>
              <option>000115 CONTAS MEDICAS</option>
              <option>000116 CREDENCIAMENTO</option>
              <option>000118 SAUDE PREVENTIVA</option>
              <option>000119 COBRANCA</option>
              <option>000121 FATURAMENTO</option>
              <option>000122 FINANCEIRO</option>
              <option>000124 OUVIDORIA</option>
              <option>000125 REGULACAO</option>
              <option>000127 ADM TAUBATE</option>
              <option>000128 CARAGUATATUBA</option>
              <option>000129 SAO SEBASTIAO</option>
              <option>000130 JACAREI</option>
              <option>000132 PINDAMONHANGABA</option>
              <option>000133 CRUZEIRO</option>
              <option>000134 RECURSOS HUMANOS</option>
              <option>000135 GUARATINGUETA</option>
              <option>00137 RECURSO DE GLOSA</option>
              <option>00139 CACHOEIRA PAULISTA</option>
              <option>00141 SECRETARIA</option>
              <option>00142 RELACIONAMENTO COM CLIENTE</option>
              <option>00143 SUPRIMENTOS</option>
              <option>00144 PARAMETRIZAÇÃO E ADEQUAÇÃO</option>
              </select>
              </div><br>
              <div class='form-group'>
              <label for='conteudo'>Conteúdo</label>
              <input type='text' name='conteudo' class='form-control' rows='3' required='required' id='data' value='".oci_result($stid, "CONTENT")."'>
              </div><br>
              <div class='form-group'>
              <label for='conteudo'>Enviar Arquivo</label>
              <input type='hidden' name='MAX_FILE_SIZE' value='250000' />
              <input class='form-control' type='file' name='fileToUpload' required='required' id='fileToUpload'>
              <span style = 'font-size: 13px;'>".oci_result($stid, "FILE_LOCAL")."</span>
              </div><br>
              <div class='form-group'>
              <button type='submit' class='btn btn-primary'>Salvar</button>
              </div>
              </form>
              ";
            }
            ?>
          </div>
        </div>
      </section>
    </main>
  </div>

  <?php include 'footer.php' ?>

</body>
</html>