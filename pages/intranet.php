<?php include '../actions/conecta.php';?>

<!DOCTYPE html>
<html lang="en">
<?php include 'head.html'?>

<body>

  <?php include 'header.php';
  include 'login/verifySessionStarted.php';
  ?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="intranet.php">intranet</a></li>
          </ol>
        </div>
      </div>
    </section>
    <section id="features" class="features">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Recursos</h2>
        </div>
        <div class="row" data-aos="fade-up" data-aos-delay="300">
          
          <div class="col-lg-3 col-md-4">
            <div class="icon-box">
            <a href="soulMv.php"><img style="width: 25%" src="https://img.swapcard.com/?u=https%3A%2F%2Fcdn-api.swapcard.com%2Fpublic%2Fimages%2Fd791dcedc51a457eba46f38d4de301bd.png&q=0.8&m=fit&w=400&h=200"></img></a>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <i class="ri-fingerprint-line" style="color: #ffbb2c;"></i>
              <h3><a href="perfil.php">Meu Perfil</a></h3>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <i class="fas fa-birthday-cake" style="color: #5578ff;"></i>
              <h3><a href="aniversariantes.php">Aniversariantes</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
            <div class="icon-box">
              <i class="fas fa-birthday-cake" style="color: #8434db;"></i>
              <h3><a href="aniversariantes_de_casa.php">Aniversariantes de Casa</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-users" style="color: #4233ff;"></i>
              <h3><a href="usuarios.php">Lista de Usuários</a></h3>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="ri-file-list-3-line" style="color: #11dbcf;"></i>
              <h3><a href="faq.php">FAQ</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-exclamation-diamond" style="color: #e361ff;"></i>
              <h3><a href="comunicados.php">Comunicados Internos</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-vector-pen" style="color: #47aeff;"></i>
              <h3><a href="RHMaster.php">RH Master</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-newspaper" style="color: #e80303;"></i>
              <h3><a href="noticias_ans.php">Notícias ANS</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-file-plus" style="color: #e80368;"></i>
              <h3><a href="formComunicados.php">Cadastrar comunicado Interno</a></h3>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-file-richtext" style="color: #ffa76e;"></i>
              <h3><a href="formFaq.php">Cadastrar FAQ</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-hand-holding-heart" style="color: #6cbbd9;"></i>
              <h3><a href="elogio.php">Cadastrar Elogios</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-heart" style="color: red;"></i>
              <h3><a href="visualiza_elogios.php">Visualizar Elogios</a></h3>
            </div>
          </div>
          
          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-user-plus" style="color: #198754;"></i>
              <h3><a href="registro_func.php">Cadastrar Novo Funcionário</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="fas fa-pencil-alt" style="color: #9e9e9e;"></i>
              <h3><a href="form_assinatura.php">Cadastrar Nova Assinatura</a></h3>
            </div>
          </div>

          <!-- <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-file-richtext" style="color: #0a58ca;"></i>
              <h3><a href="form_faq_quali.php">Cadastrar Novo FAQ Qualidade</a></h3>
            </div>
          </div> -->

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-file-richtext" style="color: #0a58ca;"></i>
              <h3><a href="sgq.php">Sistema de Gestão da Qualidade</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-book" style="color: #0a58ca;"></i>
              <h3><a href="http://portais.planosantacasasaude.santacasasjc.com.br:63499/formguiamedico/" target="_blank">Guia Médico</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-search" style="color: #0a58ca;"></i>
              <h3><a href="http://portais.planosantacasasaude.santacasasjc.com.br:60080/procedimento/index" target="_blank">Consulta de Procedimentos</a></h3>
            </div>
          </div>

          <div class="col-lg-3 col-md-4 mt-4">
            <div class="icon-box">
              <i class="bi bi-diagram-3" style="color: #0a58ca;"></i>
              <h3><a href="" target="_blank">Organograma da Empresa</a></h3>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <div style="padding-top: 1%">
    <?php include 'footer.php' ?>
  </div>

</body>

</html>