<?php include 'head.html'?>

<body>

  <?php include 'header.php';
  include 'login/verifySessionStarted.php';
  // include 'login/lvl_access.php';
  ?>

  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="comunicados.php">Comunicados</a></li>
            <?php echo '<li><a href="edita_comunicado.php?cod='.$_GET['cod'].' ">Editar Comunicado</a></li>'?>
          </ol>
        </div>
      </div>
    </section>

    <section id="team" class="team section-bg">
      <div class="container">
        <div class="section-title" data-aos="fade-up">
          <h2>Editar Comunicado</h2>
        </div>

        <div class="row">
          <div class="container">
            <div class="section-title" data-aos="fade-up"></div>
            <?php

            $sql = "SELECT extract(month from DATA_CRIACAO) AS mes, 
            extract(day from DATA_CRIACAO) AS DAY,
            extract(year from DATA_CRIACAO) AS ano,
            AUTHOR,
            COD,
            TITLE,
            FILE_LOCAL,
            CONTENT
            FROM comunicados WHERE COD = ".$_GET['cod']." ";
            $stid = oci_parse($ora_conexao, $sql) or die ("erro");
            oci_execute($stid); 

            $dia = date("d");
            $mes = date("m");
            $ano = date("y");

            while (oci_fetch($stid)) {
              echo "
              <form action='contact/update_comunicado.php' method='POST' enctype='multipart/form-data'>
              <div class='form-group'>
              <label>Autor:</label>
              <input type='hidden' name='cod' class='form-control' id='cod' value='".oci_result($stid, "COD")."'>
              <input type='text' name='autor' class='form-control' id='autor' value='".oci_result($stid, "AUTHOR")."'><br>
              </div>
              <div class='form-group'>
              <label>Titulo:</label>
              <input type='text' name='titulo' class='form-control' required='required' id='titulo' value='".oci_result($stid, "TITLE")."'><br>
              </div>
              <div class='form-group'>
              <label>Data de Edição:</label>
              <input type='text' name='data_edicao' readonly='readonly' class='form-control' required='required' id='data' value='".$dia."/".$mes."/20".$ano."'><br>
              </div>
              <div class='form-group'>
              <label for='conteudo'>Conteúdo</label>
              <input type='text' name='conteudo' class='form-control' rows='3' required='required' id='data' value='".oci_result($stid, "CONTENT")."'>
              </div><br>
              <div class='form-group'>
              <label for='conteudo'>Enviar Arquivo</label>
              <input type='hidden' name='MAX_FILE_SIZE' value='250000' />
              <input class='form-control' type='file' name='fileToUpload' required='required' id='fileToUpload'>
              <span style = 'font-size: 13px;'>".oci_result($stid, "FILE_LOCAL")."</span>
              </div><br>
              <div class='form-group'>
              <button type='submit' class='btn btn-primary'>Salvar</button>
              <button type='button'  class='btn btn-danger'><a href='comunicados.php' style='color: #fff'>Cancelar</a></button>
              </div>
              </form>
              ";
            }
            ?>
          </div>
        </div>
      </section>
    </main>
  </div>

  <?php include 'footer.php' ?>

</body>
</html>