<?php include 'head.html'?>
<!DOCTYPE html>
<html>
<script src="../assets/js/modal.js"></script>
<head>
  <link rel="stylesheet" href="../assets/css/modal.css">
</head>
<body>
  <?php include 'header.php';
  //include 'login/verifySessionStarted.php';

  $sql = "SELECT * FROM info_users WHERE CD_USUARIO LIKE UPPER('".$_SESSION['usuario']."')";
  $stid = oci_parse($ora_conexao, $sql) or die ("erro");
  oci_execute($stid);

  while (oci_fetch($stid)) {
    $_SESSION['setor'] = oci_result($stid, "LOTACAO");
  }
  ?>
  <main id="main">
    <section class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <ol>
            <li><a href="index.php">Início</a></li>
            <li><a href="comunicados.php">Comunicados</a></li>
          </ol>
        </div>
      </div>
    </section>
    <section id="comunicados" class="comunicados container">
      <div class="section-title aos-init aos-animate" data-aos="fade-up">
        <h2>Comunicados Internos</h2>
      </div>
      <form name="registar" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
        <input class="button" name="submit" type="submit" value="Buscar" />
        <div class="input"><input type="text" name="oculto" value=""></div>
      </form>
      <br>
      <div class="container">
        <div class='row'>
          <?php if (!isset($_POST['submit']) or strlen($_POST['oculto']) == 0) {

            $sql1 = "SELECT * FROM comunicados
            WHERE extract(month from data_criacao) = extract(month from sysdate)
            AND extract(day from data_criacao) = extract(day from sysdate)";

            $sql = " SELECT extract(month from DATA_CRIACAO) AS mes, 
            extract(day from DATA_CRIACAO) AS DAY,
            extract(year from DATA_CRIACAO) AS ano,
            AUTHOR,
            COD,
            TITLE,
            FILE_LOCAL,
            CONTENT
            FROM comunicados";

            $stid = oci_parse($ora_conexao, $sql) or die ("erro");
            oci_execute($stid); 

            while (oci_fetch($stid)){

              if(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") < 9){
                $data = "0".oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
              }elseif(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") > 9){
                $data = oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
              }elseif(oci_result($stid, "MES") > 9 and oci_result($stid, "DAY") < 9){
                $data = "0".oci_result($stid, "DAY")."/".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
              }else{
                $data = oci_result($stid, "DAY")."/".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
              }

              $file = explode('/', oci_result($stid, "FILE_LOCAL"));
              echo "<div class='card' style='width: 18rem;'>";
              
              if (isset($_SESSION['setor'])){
                if($_SESSION['setor'] == '000103 TI' or $_SESSION['setor'] == '000134 RECURSOS HUMANOS'){
                  echo "<div>
                  <a href='contact/deleta_comunicado.php?cod=".oci_result($stid, "COD")."' style='float: right; margin: 10px';>
                  <i class='far fa-trash-alt'></i>
                  </a>
                  <a href='edita_comunicado.php?cod=".oci_result($stid, "COD")."' style='float: right; margin-top: 10px'><i class='far fa-edit'></i></a>
                  </div>";
                }
              }

                echo"
                <img src='../".$file[5]."/".$file[6]."/".$file[7]."' class='card-img-top' alt='...' style='padding-top: 20px;'>
                <div class='card-body'>
                <h5 class='card-title'>".oci_result($stid, "TITLE")."</h5>
                <p class='card-text'>".oci_result($stid, "CONTENT")."</p>
                <p class='card-text'>Data de criação: ".$data."</p>
                <a href='../".$file[5]."/".$file[6]."/".$file[7]."' target='_blank' class='btn btn-primary'>Visualizar Anexo</a>
                </div>
                </div>
                ";}
              }elseif (isset($_POST['submit'])) {
                $sql = "SELECT extract(month from DATA_CRIACAO) AS mes, 
                extract(day from DATA_CRIACAO) AS DAY,
                extract(year from DATA_CRIACAO) AS ano,
                AUTHOR,
                COD,
                TITLE,
                FILE_LOCAL,
                CONTENT
                FROM comunicados where AUTHOR like '%".$_POST['oculto']."%'
                      or extract(day from DATA_CRIACAO) LIKE '%".$_POST['oculto']."%'
                      or CONCAT('0', extract(day from DATA_CRIACAO)) LIKE '%".$_POST['oculto']."%'
                      or extract(month from DATA_CRIACAO) LIKE '%".$_POST['oculto']."%'
                      or CONCAT('0', extract(month from DATA_CRIACAO)) LIKE '%".$_POST['oculto']."%'
                      or extract(year from DATA_CRIACAO) LIKE '%".$_POST['oculto']."%'
                      or DATA_CRIACAO like to_date('".$_POST['oculto']."', 'dd/mm')
                      or DATA_CRIACAO like to_date('".$_POST['oculto']."', 'dd/mm/yyyy')
                      or CONTENT like '%".$_POST['oculto']."%' 
                      or TITLE like '%".$_POST['oculto']."%'" ;
                $stid = oci_parse($ora_conexao, $sql) or die ("erro");

                oci_execute($stid);
                while (oci_fetch($stid)){

                  if(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") < 9){
                    $data = "0".oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
                  }elseif(oci_result($stid, "MES") < 9 and oci_result($stid, "DAY") > 9){
                    $data = oci_result($stid, "DAY")."/0".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
                  }elseif(oci_result($stid, "MES") > 9 and oci_result($stid, "DAY") < 9){
                    $data = "0".oci_result($stid, "DAY")."/".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
                  }else{
                    $data = oci_result($stid, "DAY")."/".oci_result($stid, "MES")."/".oci_result($stid, "ANO");
                  }

                  $file = explode('/', oci_result($stid, "FILE_LOCAL"));
                  var_dump($file);
                  echo "<div class='card' style='width: 18rem;'>";

                  if($_SESSION['setor'] == '000103 TI' or $_SESSION['setor'] == '000134 RECURSOS HUMANOS'){
                    echo "<div>
                    <a href='contact/deleta_comunicado.php?cod=".oci_result($stid, "COD")."' style='float: right; margin: 10px';>
                    <i class='far fa-trash-alt'></i>
                    </a>
                    <a href='edita_comunicado.php?cod=".oci_result($stid, "COD")."' style='float: right; margin-top: 10px'><i class='far fa-edit'></i></a>
                    </div>";
                  }
                  echo"
                  <img src='../".$file[4]."/".$file[5]."/".$file[6]."' class='card-img-top' alt='...' style='padding-top: 20px;'>
                  <div class='card-body'>
                  <h5 class='card-title'>".oci_result($stid, "TITLE")."</h5>
                  <p class='card-text'>".oci_result($stid, "CONTENT")."</p>
                  <p class='card-text'>Data de criação: ".$data."</p>
                  <a href='../".$file[4]."/".$file[5]."/".$file[6]."' target='_blank' class='btn btn-primary'>Visualizar Anexo</a>
                  </div>
                  </div>
                ";}
              }
            ?>
          </div>
        </div>
      </section>
    </main>

    <div style="padding-top: 9%">
      <?php include 'footer.php' ?>
    </div>
  </body>
</html>
